This is a backport of the current meta-arm main branch to dunfell. Currently, 
for dunfell purposes, only meta-arm and meta-arm-toolchain are backported.

While we do support the new ":" operator in our fork of bitbake, in order to 
maintain compatibility with dunfell, this has been backported to use the "_"
operator.

Once Oniro moves to the more current LTS, this repo will be going away.

Patches to this layer should not be sent to the meta-arm layer.

Maintainer: Eilís Ní Fhlannagáin <elizabeth.flanagan@huawei.com> 
