# TC0 specific configuration

COMPATIBLE_MACHINE = "(tc?)"
HAFNIUM_PROJECT = "reference"
HAFNIUM_INSTALL_TARGET = "hafnium"
HAFNIUM_PLATFORM = "secure_tc"

SRCREV = "464fa5a287791e7d128e37721a7d1da257144e12"
